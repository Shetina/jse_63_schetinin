package ru.t1.schetinin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskService extends IUserOwnedService<TaskDTO> {

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status);

    @NotNull
    TaskDTO changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @NotNull Status status);

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

}