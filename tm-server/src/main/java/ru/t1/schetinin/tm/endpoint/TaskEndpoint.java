package ru.t1.schetinin.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.schetinin.tm.api.endpoint.ITaskEndpoint;
import ru.t1.schetinin.tm.api.service.IServiceLocator;
import ru.t1.schetinin.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.schetinin.tm.api.service.dto.ITaskDTOService;
import ru.t1.schetinin.tm.dto.request.*;
import ru.t1.schetinin.tm.dto.response.*;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.dto.model.SessionDTO;
import ru.t1.schetinin.tm.dto.model.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.schetinin.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private IProjectTaskDTOService projectTaskDTOService;

    @NotNull
    @Autowired
    private ITaskDTOService taskDTOService;

    @NotNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        try {
            projectTaskDTOService.bindTaskToProject(userId, projectId, taskId);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        try {
            taskDTOService.changeTaskStatusById(userId, id, status);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return new TaskChangeStatusByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        try {
            taskDTOService.clear(userId);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        try {
            @Nullable final TaskDTO task = taskDTOService.create(userId, name, description);
            return new TaskCreateResponse(task);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByIdResponse showTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIdRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            @Nullable final TaskDTO task = taskDTOService.findOneById(userId, id);
            return new TaskShowByIdResponse(task);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByProjectIdResponse showTaskByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByProjectIdRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        try {
            @Nullable final List<TaskDTO> tasks = taskDTOService.findAllByProjectId(userId, projectId);
            return new TaskShowByProjectIdResponse(tasks);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        try {
            @Nullable final List<TaskDTO> tasks = taskDTOService.findAll(userId);
            return new TaskListResponse(tasks);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            taskDTOService.removeById(userId, id);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return new TaskRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        try {
            projectTaskDTOService.unbindTaskFromProject(userId, projectId, taskId);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return new TaskUnbindFromProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        try {
            taskDTOService.updateById(userId, id, name, description);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return new TaskUpdateByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIdResponse completeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIdRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            taskDTOService.changeTaskStatusById(userId, id, Status.COMPLETED);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return new TaskCompleteByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIdRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            taskDTOService.changeTaskStatusById(userId, id, Status.IN_PROGRESS);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return new TaskStartByIdResponse();
    }

}