package ru.t1.schetinin.tm.servlet;

import ru.t1.schetinin.tm.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/projects/*")
public class ProjectListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("projects", ProjectRepository.getInstance().findAll());
        req.getRequestDispatcher("/WEB-INF/views/project-list.jsp").forward(req, resp);
    }

}