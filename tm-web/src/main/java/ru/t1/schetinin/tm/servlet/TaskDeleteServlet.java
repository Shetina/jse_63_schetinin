package ru.t1.schetinin.tm.servlet;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/delete/*")
public class TaskDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        TaskRepository.getInstance().removeById(id);
        resp.sendRedirect("/tasks");
    }

}