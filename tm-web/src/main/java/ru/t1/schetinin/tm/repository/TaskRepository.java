package ru.t1.schetinin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("ONE"));
        add(new Task("TWO"));
    }

    public void create() {
        add(new Task("New Task " + System.currentTimeMillis()));
    }

    public void add(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public void save(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    @NotNull
    public Collection<Task> findAll() {
        return tasks.values();
    }

    @NotNull
    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

}