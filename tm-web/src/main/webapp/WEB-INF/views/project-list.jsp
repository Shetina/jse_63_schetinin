<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h3>PROJECT LIST</h3>

<table>
    <tr style="background-color: #558aed;">
        <th style="width: 25%;">ID</th>
        <th style="width: 20%;">Name</th>
        <th style="width: 20%;">Description</th>
        <th style="width: 10%;">Status</th>
        <th style="width: 10%;">Created</th>
        <th style="width: 5%;">Edit</th>
        <th style="width: 10%;">Delete</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td>
                <c:out value="${project.id}"/>
            </td>
            <td>
                <c:out value="${project.name}"/>
            </td>
            <td>
                <c:out value="${project.description}"/>
            </td>
            <td>
                <c:out value="${project.status.displayName}"/>
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${project.created}"/>
            </td>
            <td>
                <a href="/project/edit/?id=${project.id}"/>EDIT</a>
            </td>
            <td>
                <a href="/project/delete/?id=${project.id}"/>DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/project/create" style="padding-top: 20px;">
    <button>CREATE PROJECT</button>
</form>

<jsp:include page="../include/_footer.jsp"/>